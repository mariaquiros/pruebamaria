const {Client} = require('pg')
const client = new Client({
    user: "postgres",
    password: "postgres",
    host: "Maria",
    port: 5432,
    database: "HospitalTEC"
})

client.connect()
.then(() => console.log("Connected successfuly"))
.then(()=> client.query("select * from User"))
.then(results => console.table(results.rows))
.catch(e => console.log(e))
.finally(() => client.end())
